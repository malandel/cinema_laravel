@extends('layouts.app')

@section('title', 'add a movie')

@section('content')

<div class="container-fluid m-5">
    <h3 class="m-5 text-center">Search for a movie from the <a href="http://www.omdbapi.com/" target="_blank">OMDb API</a> and add it to your Database</a></h3>

    <div class="container d-flex justify-content-center">
        <form class="form-inline float-right" action="{{route('addMovie')}}">
            <fieldset>    
                <div class="input-group">
                        <input id="search" name="search" type="text" class="form-control" aria-label="Saisie de mots clés" required="required">
                    <div class="input-group-append">
                        <button class="btn btn-info" type="submit">Search</button>
                    </div>
                </div>  
            </fieldset> 
        </form>
    </div>
</div>

@if($movies['Search'] ?? '')

@foreach ($movies['Search'] as $movie)
<div class="card mb-5 mx-auto border border-dark" style="max-width: 800px; box-shadow: 5px 5px 15px 5px rgba(0,0,0,0.43);">
    <div class="card-header text-center border-bottom border-dark mb-3">
        <h5>{{$movie['Title']}}</h5>
    </div>
    <div class="row g-0 pb-3">
        <div class="col-md-6 text-center">
            <img src="{{$movie['Poster']}}" alt="{{$movie['Title']}}'s poster">
        </div>
        <div class="col-md-6">
            <div class="card-body">
                <p class="card-text text-muted">Year : {{$movie['Year']}}</p>
                <p class="card-text text-muted">IMDB ID : {{$movie['imdbID']}}</p>
                <div class="mx-auto" style="width: 200px;">
                    <form action="/addSuccess" method="GET">
                        <button type="submit" class="btn btn-dark">Add to database</button>
        
                    </form> 
                </div>                   
            </div>
        </div>
    </div>
</div>

@endforeach
       
@endif


@endsection