@extends('layouts.app')

@section('title', 'Home')
@section('content')

<h1 class="text-center">Modify Film</h1>
<form action="/edit" class="card form-group px-5" method="POST" style="background-color: #E0EAFF">
 @csrf

 

 <input  type="hidden" name="id" value="{{$movie['id']}}">
 <label  class="mt-3" for="title"><strong>Title:</strong></label>
 <input type="text" name="title" value="{{$movie['title']}}"  readonly><br></br>
 <label  class="mt-3" for="year"><strong>Year:</strong></label>
 <input type="text" name="year" value="{{$movie['year']}}"><br></br>
 <label class="mt-3"  for="runtime"><strong>Runtime:</strong></label>
 <input type="text" name="runtime" value="{{$movie['runtime']}}"><br></br>
 <label  class="mt-3" for="director"><strong>Director:</strong></label>
 <input type="text" name="director" value="{{$movie['director']}}"><br></br>
 <label class="mt-3" for="actors"><strong>Actors:</strong></label>
 <input type="text" name="actors" value="{{$movie['actors']}}"><br></br>
 <label class="mt-3" for="plot"><strong>Plot:</strong></label>
 <input type="text" name="plot" value="{{$movie['plot']}}"><br></br>
 <label class="mt-3" for="language"><strong>Language:</strong></label>
 <input type="text" name="language" value="{{$movie['language']}}"><br></br>
 <label class="mt-3" for="poster"><strong>Poster:</strong></label>
 <input type="text" name="poster" value="{{$movie['poster']}}"><br></br>
 <label class="mt-3" for="genre"><strong>Genre:</strong></label>
 <input type="string" name="genre" value="{{$movie['genre']}}"><br></br>
 <label class="mt-3" for="imdb_ID"><strong>imdb_ID:</strong></label>
 <input type="string" name="imdb_ID" value="{{$movie['imdb_ID']}}"  readonly><br></br>
 <label class="mt-3" for="status"><strong>Status:</strong></label>
 <input type="text b" name="status" value="{{$movie['status']}}" ><br></br>
 <button  type="submit" class="btn btn-dark mt-3 mb-3">Update</button>
 

</form>



@endsection

