@extends('layouts.app')

@section('title', 'Home')

@section('content')


    <div class="row justify-content-center mb-5">
        <div class="col-md-8">
            <div class="">


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="text-center h3">
                    {{ __('Greetings ') }}
                    <strong>{{ Auth::user()-> name }} !</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex m-3 justify-content-center">
        <div class="m-3">
            <a href="#"><button type="button" class="btn btn-outline-secondary">Return a movie</button></a>
        </div>
        @if (Auth::user()->role === 'admin')
        <div class="m-3">
            <a href="{{route('addMovie')}}"><button type="button" class="btn btn-outline-secondary">Add a movie</button></a>
        </div>
        @endif
    </div>
    <div class="d-flex flex-wrap mb-5 justify-content-center ">
        <div class="d-flex m-3">
                <p class="mx-3">Sort by :</p>

                        <a href="{{route('sortByTitle')}}"><button type="button" class="btn btn-outline-info mx-2">Title</button></a>
                        <a href="{{route('sortByYear')}}"><button type="button" class="btn btn-outline-info mx-2">Year</button></a>
                        <a href="{{route('sortByRuntime')}}"><button type="button" class="btn btn-outline-info mx-2">Runtime</button></a>
        </div>
        <div class="d-flex m-3">
                <p class="mx-3">Filter by :</p>

                        <a href="{{route('filterByAvailable')}}"><button type="button" class="btn btn-outline-info mx-2">Available</button></a>
                        <a href="{{route('filterByUnavailable')}}"><button type="button" class="btn btn-outline-info mx-2">Unavailable</button></a>
        </div>
    </div>


    @foreach ($movies as $movie)
    <div class="card mb-5 mx-auto border border-dark" style="max-width: 800px; box-shadow: 5px 5px 15px 5px rgba(0,0,0,0.43); background-color: #E0EAFF;">
        <div class="card-header text-center border-bottom border-dark mb-3">
            <h5>{{$movie->title}}

            @if($movie->status === 'available')
                <span class="badge bg-success">Available</span>
            @else
                <span class="badge bg-danger">Unavailable</span>
            @endif
            </h5>
        </div>
        <div class="row g-0 pb-3">
            <div class="col-md-6 text-center">
                <img src="{{$movie->poster}}" alt="{{$movie -> title}}'s poster">
            </div>
            <div class="col-md-6">
                <div class="card-body">
                    <p class="card-text text-muted">{{$movie-> year}} - {{$movie-> language}}</p>
                    <p class="card-text text-muted">{{$movie-> genre}}</p>
                    <p class="card-text text-muted">{{$movie-> runtime}} min - <em>Rated :</em> {{$movie-> rated}}</p>
                    <p class="card-text text-muted"><em>Director :</em> {{$movie-> director}}</p>
                    <p class="card-text text-muted"><em>Actors :</em> {{$movie-> actors}}</p>
                    <p class="card-text"><em>Plot :</em> {{$movie-> plot}}</p>
                    <div class="mx-auto" style="width: 200px;">
                        @if($movie->status === 'available')
                            <button type="button" class="btn btn-dark ml-5">Add to cart</button>
                        @endif

                      
                    </div>
                   
                </div>
                
            <!--delete movie !-->
            @if (Auth::user()->role === 'admin')
                <form action="{{ $movie -> id }}" method="POST">
               
                @method('DELETE')
                <a href={{"delete/".$movie['id']}} class="btn btn-dark" onclick="return confirm('Are you sure you want to delete this item?')">Delete Movie</a>
                <a href={{"edit/".$movie['id']}} class="btn btn-dark">Modify Movie</a>
                <button type="button" class="btn btn-dark">Loan Details</button>
                </form> 
                @endif

            </div>
        </div>
    </div>
    @endforeach

    <div class="mx-auto" style="max-width:250px;">
    {{$movies -> links()}}
    </div>



@endsection
