



<!-- Formulaire de recherche avec catégories -->

        <div class="container mb-5 mt-3">
            <form class="form-inline float-right" style="background-color: #E0EAFF" action="{{route('searchByItem')}}">
                <fieldset>    
                    <div class="input-group">
                            <input id="search" name="search" type="text" class="form-control" aria-label="Saisie de mots clés" required="required">
                        <div class="input-group-append">
                            <button class="btn btn-secondary" type="submit">Search</button>
                        </div>
                    </div>  
                </fieldset> 
            </form>
        </div>