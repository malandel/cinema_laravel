<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/', [MovieController::class, 'showMovies'])->name('home') -> middleware('auth');

Route::get('/sort=title', [MovieController::class, 'sortByTitle'])->name('sortByTitle');

Route::get('/sort=year', [MovieController::class, 'sortByYear'])->name('sortByYear');

Route::get('/sort=runtime', [MovieController::class, 'sortByRuntime'])->name('sortByRuntime');

Route::get('/filter=available', [MovieController::class, 'filterByAvailable'])->name('filterByAvailable');

Route::get('/filter=unavailable', [MovieController::class, 'filterByUnavailable'])->name('filterByUnavailable');

Route::get('/addMovie', [MovieController::class, 'addMovie'])->name('addMovie');

Route::get('/addSuccess', function(){return view('addSuccess');})->name('addSuccess');

Route::get('/addSuccess/{imdb_ID}', [MovieController::class, 'addSuccess'])->name('addSuccess');



//Hazelle
//Loans
Route::get('/loans', function() {
    return view('loans');
});

Route::get('delete/{id}', [MovieController::class,'destroy']);

Route::get('edit/{id}', [MovieController::class,'showData']);

Route::post('edit', [MovieController::class,'update']);
//     return 
// })

// searchBar route
Route::get('/search', [MovieController::class, 'searchByItem'])->name('searchByItem');

    
