<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    //for update movie to work
    public $timestamps=false;
    public function loans(){
        return $this->belongsToMany(Loan::class);
    }
    
}
