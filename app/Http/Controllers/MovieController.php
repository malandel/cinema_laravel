<?php

namespace App\Http\Controllers;
use App\Models\Movie;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;


class MovieController extends Controller
{
    function showMovies(){
       return view(
           'home', 
           [
               'movies' => Movie::simplePaginate(6)
            ]
        );
    }

    function sortByTitle(){
        return view('home', ['movies' => Movie::orderBy('title')->simplePaginate(6)]);
    }
    function sortByYear(){
        return view('home', ['movies' => Movie::orderBy('year')->simplePaginate(6)]);
    }
    function sortByRuntime(){
        return view('home', ['movies' => Movie::orderBy('runtime')->simplePaginate(6)]);
    }
    function filterByAvailable(){
        return view('home', ['movies' => Movie::where('status', 'available')->simplePaginate(6)]);
    }
    function filterByUnavailable(){
        return view('home', ['movies' => Movie::where('status', 'unavailable')->simplePaginate(6)]);
    }
    public function addMovie(Request $request){
        $input = $request->input('search');
        $askAPI = Http::get('http://www.omdbapi.com/?s='.$input.'&type=movie&apikey=f084b538');
        return view('addMovie', ['movies' => $askAPI]);
    }
    function addSuccess($imdb_ID){
        $recherche = Http::get('http://www.omdbapi.com/?i='.$imdb_ID.'&apikey=f084b538');
        DB::table('movies')->insert([
            'title' => $recherche['Title'],
            'year' => (int)$recherche['Year'],
            'runtime' => (int)str_replace(' min', "", $recherche['Runtime']),
            'director' => $recherche['Director'],
            'actors' => $recherche['Actors'],
            'plot' => $recherche['Plot'],
            'language' => $recherche['Language'],
            'poster' => $recherche['Poster'],
            'genre' => $recherche['Genre'],
            'rated' => $recherche['Rated'],
            'imdb_ID' => $recherche['imdbID']
        ]);
        return view('addSuccess');
    }

    // searchBar Controller
    function searchByItem(Request $request){
 
         $search = $request->input('search');
        
        return view('home', ['movies' => Movie::where('title', 'like', '%'.$search.'%')->orWhere('plot', 'like', '%'.$search.'%')->orWhere('year', 'like', '%'.$search.'%')->simplePaginate(6)]);
    }

    //Delete movie
     function destroy($id) {
       
        $movie = Movie::findOrFail($id);
        $movie -> delete();
        

        return redirect('/');

    }

    // Modify movie 

    function showData($id) {

        $movie = Movie::findOrFail($id);
        return view('edit', ['movie' => $movie]);
    }
// Validate movie
    function update(Request $req) {

        // return $req -> input();

        $movie=Movie::findOrFail($req -> id);
        // $movie-> title = $req ->title;
        $movie -> year = $req ->year;
        $movie -> runtime = $req ->runtime;
        $movie -> director = $req ->director;
        $movie -> actors = $req ->actors;
        $movie -> plot = $req ->plot;
        $movie -> language = $req ->language;
        $movie -> poster = $req ->poster;
        $movie -> genre = $req ->genre;
        // $movie -> imdb_ID = $req -> validate(['imb_ID' => 'required|string']);
        $movie -> status = $req ->status;

        $movie->save();
        return redirect('/');

       

    }

}
