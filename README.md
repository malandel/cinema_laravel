# Project application : Cinema with Laravel

## Local installation

**We are using the GitFlow method : the 'master' branch is used for production (Heroku deployment) and 'dev' is used for development. You'll have to work from the 'dev' branch if you want your app to work on your local environment.** 

- Clone the project and install dependencies
```
cd cinema
composer install && npm install
```
- In PHPMyAdmin, create a "laravel-cinema" database with 'utf8mb4-unicode-ci'

- Create a new .env file from the .env.example file at the root of your project with this command :```cp .env.example .env```
- Then put your personal login and password for your PHPMyAdmin : DB_USERNAME=*yourdb_username* , DB_PASSWORD=*yourdb_password*
- Generate your APP KEY : ```php artisan key:generate```
- Run the migrations to create the tables, then seed them : ```php artisan migrate:fresh && php artisan db:seed --class=UsersSeeder && php artisan db:seed --class=MoviesSeeder```
- create your branch from the 'dev' branch (GitlabFlow)
- The diagrams are in the DIAGRAMS folder
- Run the server with : ``` php artisan serve ```

## Deploy with Heroku

- Make sure you're on the master branch ```git checkout master```

- If it's not already done, create a free account on https://signup.heroku.com/login 

- Install the Heroku Command Line Interface (CLI) : ```sudo snap install heroku --classic```

- Make sure that a *Procfile* file is at the root of your app and contains the following line : 'web: vendor/bin/heroku-php-apache2 public/'

- If not : ```echo "web: vendor/bin/heroku-php-apache2 public/" > Procfile```

- Log in with the Heroku CLI : ``` heroku login ``` and follow the instructions

- Then create a remote to Heroku : ```heroku create``` 

- Open your .env file created earlier, and copy your APP_KEY

- Paste it to replace 'yourAppKey : ``` heroku config:set APP_KEY=yourAppKey```

- BUT you have to connect the MySql database now ! You need the [ClearDB MySQL](https://devcenter.heroku.com/articles/cleardb) addon provided by Heroku. There's a free plan, but it requires you to verify your account by filling your billing infos (Account settings/Billing/Billing Informations) -> https://dashboard.heroku.com/account/billing

- Add ClearDB to your project and config it

```
heroku addons:create cleardb:ignite
heroku config | grep CLEARDB_DATABASE_URL
```
- Copy the value of the CLEARDB_DATABASE_URL config variable and paste it ```heroku config:set DATABASE_URL='paste_here'```

- Open *config/database.php* and make sure you have the following code in it : 

```
//At the top of your file :
$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
$host = $url["host"] ?? null;
$username = $url["user"] ?? null;
$password = $url["pass"] ?? null;
$database = substr($url["path"], 1);

// Just after the return [ :
'default' => env('DB_CONNECTION', 'your_heroku_mysql_connection'),

// In the 'connections' array :
'your_heroku_mysql_connection' => array(
            'driver' => 'mysql',
            'host' => $host,
            'database' => $database,
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ),
```

- Now push to heroku : ```git push heroku master```

- Run the migrations and seeders : ```heroku run php artisan migrate && heroku run php artisan db:seed```

- Open your online app with ```heroku open```


## Figma wireframe

[Wireframe](https://www.figma.com/file/JwWQTYqoQbdCUqFONvMbU1/Laravel-cinema?node-id=1%3A6081) 

## Asana board

[Asana](https://app.asana.com/0/1199921754833391/board)

## Online address

http://cinema-laravel.herokuapp.com


## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
