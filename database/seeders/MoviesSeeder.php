<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;


class MoviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dog = Http::get('http://www.omdbapi.com/?s=dog&type=movie&apikey=f084b538');
            foreach($dog['Search'] as $film){
                $recherche = Http::get('http://www.omdbapi.com/?i='.$film['imdbID'].'&apikey=f084b538');
                    DB::table('movies')->insert([
                        'title' => $recherche['Title'],
                        'year' => (int)$recherche['Year'],
                        'runtime' => (int)str_replace(' min', "", $recherche['Runtime']),
                        'director' => $recherche['Director'],
                        'actors' => $recherche['Actors'],
                        'plot' => $recherche['Plot'],
                        'language' => $recherche['Language'],
                        'poster' => $recherche['Poster'],
                        'genre' => $recherche['Genre'],
                        'rated' => $recherche['Rated'],
                        'imdb_ID' => $recherche['imdbID']
                    ]);
            };   
        $bat = Http::get('http://www.omdbapi.com/?s=bat&type=movie&apikey=f084b538');
            foreach($bat['Search'] as $film){
                $recherche = Http::get('http://www.omdbapi.com/?i='.$film['imdbID'].'&apikey=f084b538');
                    DB::table('movies')->insert([
                        'title' => $recherche['Title'],
                        'year' => (int)$recherche['Year'],
                        'runtime' => (int)str_replace(' min', "", $recherche['Runtime']),
                        'director' => $recherche['Director'],
                        'actors' => $recherche['Actors'],
                        'plot' => $recherche['Plot'],
                        'language' => $recherche['Language'],
                        'poster' => $recherche['Poster'],
                        'genre' => $recherche['Genre'],
                        'rated' => $recherche['Rated'],
                        'imdb_ID' => $recherche['imdbID']
                    ]);
            }     
    }
}
